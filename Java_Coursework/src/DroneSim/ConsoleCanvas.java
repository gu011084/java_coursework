package DroneSim;

public class ConsoleCanvas {
	
	char[][] Canvas; 
	private int cx, cy;
	
	//sets the size of the canvas and creates the border using #

	public ConsoleCanvas(int x, int y) {
		cx = x+3;
		cy = y+3;
		Canvas = new char[cx][cy];
		for(int i = 0; i < cx; i++) {
			Canvas[i][0] = '#';
			Canvas[i][y+2] = '#';
		}	
		for(int j = 0; j < cy; j++) {
			Canvas[0][j] = '#';
			Canvas[x+2][j] = '#';		
		}
	}
	
	//places a D in a position where a drone is

	public void showIt(int x, int y, char d) {
		Canvas[x+1][y+1] = d;	
	}
	
	//removes D from its position so it can be moved
	
	public void DeleteIt(int x, int y) {
		Canvas[x+1][y+1] = ' ';
	}
	
	//prints entire arena in string format
	
	public String toString() {
		
		String StrCanvas = "";
		for (int k = 0; k < cy; k++) {
			for (int l = 0; l < cx; l++) {
				StrCanvas = StrCanvas + Canvas[l][k];
			}
			StrCanvas = StrCanvas + "\n";
		}
		return StrCanvas;
	}
	
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas(56, 32);
		//c.showIt(0, 0,'D');
		//c.showIt(56, 32, 'E');
		//c.showIt(0, 32, 'F');
		//c.showIt(56, 0, 'G');
		System.out.println(c.toString());
	}
}
