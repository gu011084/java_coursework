package DroneSim;
import java.util.Random;
import java.util.ArrayList;

public class DroneArena {
	
	private static int width, height, val1, val2;
	private static Drone d, check;	
	static ArrayList<Drone> Dronelist;
	
	public DroneArena(int Aw, int Ah) {
		width = Aw;
		height = Ah;
		Dronelist = new ArrayList<Drone>();
		
	}
	
	//Displays all the drones on the canvas in their correct positions
	
	public void showDrones(ConsoleCanvas c) {
		for (Drone number : Dronelist ) {
			number.displayDrone(c);
		}
	}
	
	//creates a new drone and adds it to the drone list with a unique direction and x y position
	
	public Drone addDrone() {
		do {
			Random randGen;
			randGen = new Random();
			val1 = randGen.nextInt(width);
			val2 = randGen.nextInt(height);
		
			d = new Drone(val1, val2, Direction.getDirection());
			check = getDroneAt(val1, val2);
			
		}while(check == null);
		Dronelist.add(d);
		return d;
	}
	
	//Displays the details of the arena and every drone in the drone list
	
	public String toString() {
		String str = "Drone Arena is " + width + " by " + height + "\n";
		for(Drone number : Dronelist) {
			System.out.println(number);
		}
		return str;
	}
	
	//checks if any drone is at a certain position in the arena
	
	public Drone getDroneAt(int x, int y) {
		for(Drone exist : Dronelist ) {
			if (exist.isHere(x, y)) {
				return null;
			}
		}
		return d;
	}
	
	//checks if a drone can move to a specific spot
	
	public static boolean canMoveHere(int newx, int newy, ConsoleCanvas c) {
		if (Dronelist != null) {
			for(Drone search : Dronelist) {
				if (search.isHere(newx, newy)) {
					return false;
				}
			}
		}
		
		//checks if the drone is hitting the edge of the arena
		
		if (c.Canvas[newx+1][newy+1] == '#') {
			return false;
		}
		else {
			return true;
		}
		
		
	}
	
	//moves all drones one position in their set direction
	
	public void moveAllDrones(ConsoleCanvas c) {
		for(Drone move : Dronelist) {
			move.tryToMove(c);
		}
	}
	
	
	public static void main(String[] args) {
		DroneArena a = new DroneArena(20, 10);
		for (int i = 0; i < 20; i++) {
			a.addDrone();
		}
		System.out.println(a.toString());
	}
}
