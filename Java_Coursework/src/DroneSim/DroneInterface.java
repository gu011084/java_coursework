package DroneSim;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class DroneInterface {
	private int Aw, Ah;
	private Scanner s;
	private DroneArena myArena;
	private ConsoleCanvas c;
	
	public DroneInterface() {
		
		//allows user input using scanner to allow the user to control aspects of the arena
		
		s = new Scanner(System.in);
		setDisplay();
		
		char ch = ' ';
		do {
			
			System.out.print("Enter (C)reate new arena, Enter (A)dd drone, get (I)nformation,\n (D)isplay arena, (M)ove drones, a(N)imate, (S)ave simulation or e(x)it : ");
			
			ch = s.next().charAt(0);
			s.nextLine();
			switch(ch) {
			case 'C' :
			case 'c' :
				setDisplay();
				break;
			case 'A' :
			case 'a' :
				myArena.addDrone();
				break;
			case 'I' :
			case 'i' :
				System.out.println(myArena.toString());
				break;
			case 'D' :
			case 'd' :
				doDisplay();
				break;
			case 'M' :
			case 'm' :
				myArena.moveAllDrones(c);
				break;
			case 'N' :
			case 'n' :
				Animation();
				break;
			case 'S' :
			case 's' :
				break;
			case 'x' : ch = 'X';
			break;
			}
		} while(ch != 'X');
		s.close();
	}
	
	//enables the user to set the width and height of the arena
	
	void setDisplay() {
		System.out.print("Enter Arena width : ");
		Aw = s.nextInt();
		s = new Scanner(System.in);
		System.out.print("Enter Arena height : ");
		Ah = s.nextInt();
		myArena = new DroneArena(Aw, Ah);	
		c = new ConsoleCanvas(Aw, Ah);	
	}
	
	//displays the arena
	
	void doDisplay() {	
		myArena.showDrones(c);
		System.out.println(c.toString());
	}
	
	//animates the arena to see the drones move around 
	
	void Animation() {
		
		boolean Stop = false;
		while (Stop == false) {
			//c.clearCanvas();
			myArena.showDrones(c);
			System.out.println(c.toString());
			myArena.moveAllDrones(c);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();
	}
}