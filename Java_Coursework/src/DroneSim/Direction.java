package DroneSim;
import java.util.Random;

//Stores the possible directions which the drone can move

public enum Direction {
	North,
	East,
	South,
	West,
	SE,
	NE,
	SW,
	NW;
	
	//creates a random direction to be assigned to a drone
	
	public static Direction getRandomDir() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}	
		
	//Allows the direction to be used in another class (Drone class)
	
	public static Direction getDirection() {
		return Direction.getRandomDir();
	}
	
	public static void main(String[] args) {
	}	
}
