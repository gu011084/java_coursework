package DroneSim;

public class Drone {
	
	private int x, y, DroneID;
	private static int count = 0;
	private Direction dir;
	
	public Drone(int cx, int cy, Direction nesw) {
		x = cx;
		y = cy;
		DroneID = count++;
		dir = nesw;
	}
	
	//returns the direction,x and y position of the drone in string format
	
	public String toString() {
		return "Drone " + DroneID + " is at " + x + ", " + y + " going " + dir;
	}
	
	//Checks if there is the drone is at a specific spot in the arena
	
	public boolean isHere (int sx, int sy) {
		if (sx == x && sy == y) {
			return true;
		}else {
			return false;
		}
		
	}
	
	//Displays the Drone at x y 
	
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(x, y, 'D');
	}
	
	//Checks if the Drone can move in given direction
	
	public void tryToMove(ConsoleCanvas c) {
		boolean Moved = false;
		while (Moved == false) {
			int newx = x;
			int newy = y;
			if (dir == Direction.North) {newy++;}
			else if (dir == Direction.South) {newy--;}
			else if (dir == Direction.East) {newx++;}
			else if (dir == Direction.West) {newx--;}
			else if (dir == Direction.SE) {newy--;newx++;}
			else if (dir == Direction.NE) {newy++;newx++;}
			else if (dir == Direction.SW) {newy--;newx--;}
			else if (dir == Direction.NW) {newy++;newx--;}
			if((DroneArena.canMoveHere(newx, newy, c)) == true) {
				c.DeleteIt(x, y);
				x = newx;
				y = newy;	
				Moved = true;
			}
			else {
				dir = Direction.getRandomDir();
				Moved = false;
			}
		}
	}
	
	public static void main(String[] args) {
		Drone d = new Drone(0, 0, Direction.getDirection());
		System.out.println(d.toString());
	}

}
